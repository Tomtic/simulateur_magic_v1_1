#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:46:22 2020

@author: rayricher
"""

def valider_cout_mana(action, manapool, cout_mana):
        
    #On vérifie dans les deux prochains conditionnel si la mana fournit
    #correspond minimalement au manacost de l'action nécéssaire
    if not (set(action.symbole).issubset(set(cout_mana.manaliste))):
        return False  
      
    #On vérifie que la mana est bien disponible dans la réserve de mana
    elif cout_mana.mana.get('W') > manapool.mana.get('W'):
        return False

    elif cout_mana.mana.get('B') > manapool.mana.get('B'):
        return False
        
    elif cout_mana.mana.get('R') > manapool.mana.get('R'):
        return False

    elif cout_mana.mana.get('G') > manapool.mana.get('G'):
        return False
            
    elif cout_mana.mana.get('U') > manapool.mana.get('U'):
        return False    

    elif cout_mana.mana.get('C') > manapool.mana.get('C'):
        return False
    
    else:
        return True