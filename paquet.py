#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 00:17:56 2020

@author: rayricher
"""

from fichier_carte import *

class Paquet:
    
    def __init__(self, nom):
        
        self.nom = nom
        self.liste_paquet = []
        self.liste_construction = []
    
    """
    Permet d'ajouter une carte au paquet, La liste construction sert lors de la 
    conception du paquet alors que la liste jeux sert aux manipulations durant
    une partie. 
    """
    
    def ajouter_carte(self, carte, nb=1):
        
        self.liste_paquet += nb*[carte]
        self.liste_construction += [(nb, carte)]
    
    def liste(self):
        
        print(self.nom, ':')
        for i in self.liste_construction:
            print('- {}x {}'.format(i[0], i[1].nom))
            